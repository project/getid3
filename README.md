# getID3()

The getID3() Drupal module facilitates the installation and management of the
getID3() PHP library, used to extractuseful information from MP3s and other
multimedia file formats.

getID3() is a PHP script that extracts useful information (such as ID3 tags,
bitrate, playtime, etc.) from MP3s & other multimedia file formats (Ogg, WMA,
WMV, ASF, WAV, AVI, AAC, VQF, FLAC, MusePack, Real,QuickTime, Monkey's Audio,
MIDI and more).

This API module is used by other modules to ensure that getID3() is correctly
installed. Developers who need the getID3() functionality can make this a
dependency by their module, and then call the getid3_load() function to properly
use the getID3() library without having to worry about whether it's installed
or not.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/getid3).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/getid3).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following packages:

- [james-heinrich/getid3](https://github.com/JamesHeinrich/getID3)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Dashboard > Modules and enable the GetID3 module.
2. Go to Dashboard > Configuration > Media > getID3. If you see the version you
   just downloaded, the module is installed correctly.


## Maintainers

- Aaron Winborn - [aaron](https://www.drupal.org/u/aaron)
- Alex - [a_c_m](https://www.drupal.org/u/a_c_m)
- andrew morton - [drewish](https://www.drupal.org/u/drewish)
- Eric J. Duran - [ericduran](https://www.drupal.org/u/ericduran)
- Jason Blanda - [jason.blanda](https://www.drupal.org/u/jasonblanda)
- John Albin Wilkins - [johnalbin](https://www.drupal.org/u/johnalbin)
- Matt Glaman - [mglaman](https://www.drupal.org/u/mglaman)
- Nate Lampton - [quicksketch](https://www.drupal.org/u/quicksketch)
- Rob Loach - [robloach](https://www.drupal.org/u/robloach)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
