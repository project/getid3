<?php

namespace Drupal\getid3\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form builder for the GetId3 settings form.
 *
 * @internal
 */
class GetId3ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'getid3_systemconfigformbase';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'getid3.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $path = getid3_get_path();
    $form['getid3_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#default_value' => $path,
      '#description' => $this->t('The location where getID3() is installed. Relative paths are from the Drupal root directory.'),
    ];

    $form['getid3_path']['#after_build'][] = [get_class($this), 'afterBuild'];

    if ($version = getid3_get_version()) {
      $form['getid3_version'] = [
        '#type' => 'item',
        '#title' => $this->t('Version'),
        '#markup' => '<pre>' . Html::escape($version) . '</pre>',
        '#description' => $this->t("If you're seeing this it indicates that the getID3 library was found."),
      ];

      // Check for existence of the 'demos' folder, contained in the getID3
      // library. The contents of this folder create a potential security hole,
      // so we recommend that the user delete it.
      $getid3_demos_path = $path . '/../demos';
      if (file_exists($getid3_demos_path)) {
        $this->messenger()
          ->addError($this->t("Your getID3 library is insecure! The demos distributed with getID3 contains code which creates a huge security hole. Remove the demos directory (%path) from beneath Drupal's directory.", ['%path' => realpath($getid3_demos_path)]));
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Save the new value.
    $this->config('getid3.settings')
      ->set('path', $form_state->getValue('getid3_path'))
      ->save();
  }

  /**
   * Verifies that getid3 is in the directory specified by the form element.
   *
   * Checks that the directory in $form_element exists and contains files named
   * 'getid3.php' and 'write.php'. If validation fails, the form element is
   * flagged with an error.
   *
   * @param array $form_element
   *   The form element containing the name of the directory to check.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns form element.
   */
  public static function afterBuild(array $form_element, FormStateInterface $form_state) {
    $path = $form_state->getValue('getid3_path');
    if (!is_dir($path) || !(file_exists($path . '/getid3.php') && file_exists($path . '/write.php'))) {
      \Drupal::messenger()
        ->addError(t('The getID3 files <em>getid3.php</em> and <em>write.php</em> could not be found in the %path directory.', ['%path' => $path]));
    }
    return $form_element;
  }

}
